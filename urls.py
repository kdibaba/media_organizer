from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^tv/', include('tv_organizer.urls')),
    url(r'^movie/', include('movie_organizer.urls')),
    url(r'^file_transfers/', include('file_transfers.urls')),
)
