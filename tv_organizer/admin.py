from django.contrib import admin
from django.db import models
from tv_organizer.models import (TvDrive,
                                 TvCatalog,
                                 TvShow,
                                 TvMissing,
                                 TvRenameOptions)

import os

class TvDriveAdmin(admin.ModelAdmin):
    list_display = ('name',
                    'path', 
                    'get_capacity',
                    'get_free_space',
                    'enabled')
    def get_free_space(self, obj):
        if obj.free_space:
            return str(int(obj.free_space) / (1024*1024*1024)) + ' GB'
        else:
            return 'Not yet scanned'

    def get_capacity(self, obj):
        if obj.capacity:
            return str(int(obj.capacity) / (1024*1024*1024)) + ' GB'
        else:
            return 'Not yet scanned'
admin.site.register(TvDrive, TvDriveAdmin)

class TvRenameOptionsAdmin(admin.ModelAdmin):
    list_display = ('name',
                    'characters_to_replace',
                    'replace_with_this', )
admin.site.register(TvRenameOptions, TvRenameOptionsAdmin)

class TvCatalogAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'show',
                    'season',
                    'episode',
                    'quality',
                    'readable_file_size',
                    'duplicate',
                    'to_delete',
                    'file_name',)
    search_fields = ['show__name']
    actions = ['mark_to_delete', 'remove_delete_mark']
    list_max_show_all = 10000
    class Media:
        css = {'all': ('css/admin/base.css',)}

    def mark_to_delete(self, request, queryset):
        for episode in queryset:
            episode.to_delete = True
            episode.save()
        self.message_user(request, 'successfully marked {0} episodes for deletion'.format(len(queryset)))

    def remove_delete_mark(self, request, queryset):
        for episode in queryset:
            episode.to_delete = False
            episode.save()
        self.message_user(request, 'successfully removed mark from {0} episodes for deletion'.format(len(queryset)))

    def readable_file_size(self, obj):
        if obj.file_size:
            return str(int(obj.file_size) / (1024*1024)) + ' MB'
admin.site.register(TvCatalog, TvCatalogAdmin)

class TvCatalogInline(admin.TabularInline):
    model = TvCatalog
    ordering = ("file_name",)
    extra = 0

class TvMissingInline(admin.TabularInline):
    model = TvMissing
    extra = 0

class TvMissingAdmin(admin.ModelAdmin):
    list_display = (
        'show',
        'season',
        'episode',
        'missing_season',
        'iptorrents',
    )
    def iptorrents(self, obj):
        if obj.missing_season:
            return '<a href="{}{} s{} 720p" target="_blank">{}</a>'.format(
                'https://iptorrents.com/t?q=',
                obj.show,
                str(obj.season).zfill(2),
                'ipt')
        else:
            return '<a href="{}{} s{}e{} 720p" target="_blank">{}</a>'.format(
                'https://iptorrents.com/t?q=',
                obj.show,
                str(obj.season).zfill(2),
                str(obj.episode).zfill(2),
                'ipt')

    iptorrents.allow_tags = True
admin.site.register(TvMissing, TvMissingAdmin)

class TvShowAdmin(admin.ModelAdmin):
    list_display = ('name', 'episodes')
    inlines = [
        TvCatalogInline,
        TvMissingInline,
    ]
    search_fields = ['name']
admin.site.register(TvShow, TvShowAdmin)
