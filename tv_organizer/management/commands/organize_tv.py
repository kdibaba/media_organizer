from django.core.management.base import BaseCommand, CommandError
from tv_organizer.models import (TvDrive,
                                 TvCatalog,
                                 TvShow,
                                 TvMissing,
                                 TvRenameOptions,)
from optparse import make_option
from django.utils import timezone
from django.db.models import Count
import os, shutil, re
from unicodedata import normalize

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
         make_option('--verbose',
             dest='verbose',
             action='store_true',
             help='Verbose mode. Display all actions on screen.'),
         make_option('--process-drives',
             dest='process_drives',
             action='store_true',
             help='Rename files and move them to the right directories'),
         make_option('--catalog',
             dest='catalog',
             action='store',
             help='Catalog the tv_shows in the collection'),
         make_option('--dry-run',
             dest='dry_run',
             action='store_true',
             help='Dry run.. nothing is executed.'),
         make_option('--find-missing-episodes',
             dest='find_missing_episodes',
             action='store_true',
             help='Find and mark missing eps'),
         make_option('--find-duplicates',
             dest='find_duplicates',
             action='store_true',
             help='Find and mark duplicates'),
         make_option('--delete-duplicates',
             dest='delete_duplicates',
             action='store_true',
             help='Delete duplicates'),
         )
    def handle(self, *args, **options):
        self.verbose = options.get('verbose', False)
        self.process_drives = options.get('process_drives', False)
        self.catalog = options.get('catalog', False)
        self.dry_run = options.get('dry_run', False)
        self.find_missing_episodes = options.get('find_missing_episodes', False)
        self.find_duplicates = options.get('find_duplicates', False)
        self.delete_duplicates = options.get('delete_duplicates', False)
        self.catalog_extension_blacklist = ['idx', 'srt', 'sub']

        self.quality_score = {
            '1080p':10,
            '720p': 9,
            'dvdrip': 8,
            'hdrip': 7,
            'pdtv': 6,
            '480p': 6,
            'dsrip': 5,
            'tvrip': 4,
            'unknown': 3,
            'vcd': 2,
        }
        self.verbose_print(u'Getting all settings from the organizer')
        if self.catalog == 'new':
            TvCatalog.objects.all().delete()
            TvShow.objects.all().delete()
            TvMissing.objects.all().delete()

        if self.process_drives:
            drives = TvDrive.objects.filter(enabled=True)
            self.verbose_print(u'Got {0} setting(s) and processing them now\n'.format(drives.count()))
            if drives:
                for drive in drives:
                    self.update_drive_stats(drive)
                    files_to_rename = self.get_file_names(drive.path)
                    if files_to_rename:
                        self.rename_files(drive, files_to_rename)
                        files_to_move = self.get_file_names(drive.path)
                        if files_to_move:
                            self.move_and_catalog_files(files_to_move, drive)
                        else:
                            self.verbose_print(u'Found no files to move. Very unlikely.')
                            drive.last_message = 'Found no files to move. Very unlikely'
                            drive.save()
                    else:
                        self.verbose_print(u'Found no files to process for setting: {0}'.format(drive.name))
                        drive.last_message = 'No files to process'
                        drive.save()
            else:
                self.verbose_print(u'No drives to process')

        if self.find_duplicates:
            self.verbose_print(u'\nLooking for and marking duplicates')
            self.find_mark_duplicates()

        if self.delete_duplicates:
            self.delete_marked_duplicates();

        if self.find_missing_episodes:
            self.verbose_print(u'Looking for and marking missing episodes')
            self.find_mark_missing_eps()


    def find_mark_missing_eps(self):
        shows = TvCatalog.objects.filter(
            show__name__in=[
                'underbelly',
                'under the dome',
                'hack my life',
                'adventure time with finn and jake',
                'house',
                'skins'
                ]
            ).values_list('show', flat=True).distinct()
        shows = TvCatalog.objects.values_list('show', flat=True).distinct()
        TvMissing.objects.all().delete()


        missing_seasons = []
        missing_episodes = []
        for show in shows:
            episodes = TvCatalog.objects.filter(show=show).order_by('-file_name', '-episode')
            current_season = episodes[0].season
            current_episode = episodes[0].episode
            new_season = False
            if not current_season or not current_episode:
                self.verbose_print(u'no season or episode. Skipping this show - {}'.format(show))
                continue

            for episode in episodes:
                if not episode.season or not episode.episode:
                    self.verbose_print(u'no season or episode. Skipping this episode - {}'.format(episode.file_name))
                    continue
                if new_season:
                 #   print 'its a new season'
                    if current_season < episode.season:
                        continue
                    if current_season != episode.season:
                        while current_season > episode.season:
                            missing_episode, created = TvMissing.objects.get_or_create(
                                show=episode.show,
                                episode=0,
                                season=current_season,
                                drive=episode.drive,
                                missing_season=True
                            )
                            current_season -= 1
                    new_season = False
                    current_episode = episode.episode

                #print 'current season - {}'.format(current_season)
                #print 'current episode - {}'.format(current_episode)
                #print 'episode episode - {}'.format(episode.episode)
                if current_season == episode.season and current_episode == episode.episode:
                    if episode.episode == 1:
                        if current_season > 1:
                            current_season -= 1
                        else:
                            break
                        new_season = True
                    else:
                        current_episode -= 1
                else:
                    neither_ifs_triggered = True
                    if current_season != episode.season:
                        neither_ifs_triggered = False
                #        print 'in season check'
                        while current_episode >= 1:
                            missing_episode, created = TvMissing.objects.get_or_create(
                                show=episode.show,
                                episode=current_episode,
                                season=current_season,
                                drive=episode.drive,
                                missing_season=False
                            )
                            current_episode -= 1
                        current_episode = episode.episode
                        new_season = True
                        current_season -= 1
                #        print '  season check current season - {}'.format(current_season)
                #        print '  season check current episode - {}'.format(current_episode)
                #        print '  season check episode episode - {}'.format(episode.episode)
                    if current_episode != episode.episode:
                        neither_ifs_triggered = False
                        while current_episode != episode.episode:
                #            print ' in while loop current episode - {}'.format(current_episode)
                #            print ' in while loop episode episode - {}'.format(episode.episode)
                            if current_episode == 1:
                                break
                            missing_episode, created = TvMissing.objects.get_or_create(
                                show=episode.show,
                                episode=current_episode,
                                season=current_season,
                                drive=episode.drive,
                                missing_season=False
                            )
                            current_episode -= 1
                #            print ' in while loop after current episode - {}'.format(current_episode)
                #            print ' in while loop after episode episode - {}'.format(episode.episode)
                        current_episode -= 1
                    if neither_ifs_triggered:
                        current_episode -= 1
                    if current_episode < 1:
                #        print 'current episode is at 1'
                        if current_season > 1:
                            current_season -= 1
                        else:
                #            print 'breaking..................'
                            break
                        new_season = True
            if current_episode > 1:
                while current_episode >= 1:
                    missing_episode, created = TvMissing.objects.get_or_create(
                        show=episode.show,
                        episode=current_episode,
                        season=current_season,
                        drive=episode.drive,
                        missing_season=False
                    )
                    current_episode -= 1
                current_season -= 1
            if current_season > 1 or new_season:
                while current_season >= 1:
                    missing_episode, created = TvMissing.objects.get_or_create(
                        show=episode.show,
                        episode=0,
                        season=current_season,
                        drive=episode.drive,
                        missing_season=True
                    )
                    current_season -= 1

    def find_mark_duplicates(self):
        TvCatalog.objects.filter(to_delete=True).update(to_delete=None)
        TvCatalog.objects.filter(duplicate=True).update(duplicate=False)
        all_duplicate_episodes = TvCatalog.objects.filter(
                                    to_delete=None).values(
                                    'show', 'season', 'episode').annotate(
                                    Count('id')).order_by().filter(
                                    id__count__gt=1)
        duplicate_exceptions = ['.part','.cd']
        for episode_dict in all_duplicate_episodes:
            if episode_dict['season'] == 0 or episode_dict['episode'] == 0:
                continue
            duplicate_episodes = TvCatalog.objects.filter(
                show__id=episode_dict['show'],
                season=episode_dict['season'],
                episode=episode_dict['episode'],
            )
            for episode in duplicate_episodes:
                skip_episode = False
                for exception in duplicate_exceptions:
                    if exception in episode.file_name:
                        skip_episode = True
                if skip_episode == False:
                    duplicate_episodes.update(duplicate=True)
                    delete_episode = ''
                    for episode in duplicate_episodes:
                        if not delete_episode:
                            delete_episode = episode
                        elif episode.quality == 'unknown' or delete_episode.quality == 'unknown':
                            #print 'quality unknown got here ' + episode.file_name
                            if int(episode.file_size) < int(delete_episode.file_size):
                                delete_episode = episode
                            elif int(episode.file_size) == int(delete_episode.file_size):
                                if self.quality_score[episode.quality] < self.quality_score[delete_episode.quality]:
                                    delete_episode = episode
                        elif self.quality_score[episode.quality] == self.quality_score[delete_episode.quality]:
                            # if the quality is unknown or have the same score we should just go with the bigger file size
                            #print 'equal score got here ' + episode.file_name
                            if int(episode.file_size) < int(delete_episode.file_size):
                                delete_episode = episode
                        elif self.quality_score[episode.quality] < self.quality_score[delete_episode.quality]:
                            delete_episode = episode
                    if delete_episode.to_delete == None:
                        delete_episode.to_delete = True
                        delete_episode.save()
                else:
                    TvCatalog.objects.filter(show=episode.show, season=episode.season, episode=episode.episode).update(duplicate=False)

    def update_drive_stats(self, drive):
        self.verbose_print(u'update drive information for {0}'.format(drive.name))
        drive_info = os.statvfs(drive.path)
        capacity = drive_info.f_blocks * drive_info.f_frsize
        free_space = drive_info.f_bavail * drive_info.f_frsize
        self.verbose_print(u'{0} has {1} space out of {2}'.format(drive.name, free_space, capacity))

        drive.capacity = capacity
        drive.free_space = free_space
        if not self.dry_run:
            drive.save()
        return

    def delete_marked_duplicates(self):
        episodes = TvCatalog.objects.filter(to_delete=True)
        for episode in episodes:
            self.verbose_print(u'{} deleted for being a duplicate'.format(episode.file_name))
            if not self.dry_run:
                try:
                    os.remove(episode.file_path)
                    episode.delete()
                except:
                    self.verbose_print('ERROR deleting file {}'.format(episode.file_path))

    def verbose_print(self, message):
        if self.verbose:
            print message

    def get_file_names(self, directory):
        filenames = []
        file_list = []
        invalid_file_types = ['db', 'ini', 'nfo', 'sfv', 'rar', 'png', 'mp3', 'm3u', 'jpg', 'tbn', 'log', 'dat']
        for root, dirs, files in os.walk(directory):
            for name in files:
                extension = name.rsplit('.')[-1]
                if name[0] != '.':
                    if extension.lower() not in invalid_file_types:
                        file_list.append(os.path.join(root, name))
                    else:
                        self.verbose_print(u'Deleting invalid file type {0}'.format(normalize('NFKD', os.path.join(root,name)).encode('ascii','ignore')))
                        if not self.dry_run:
                            os.remove(os.path.join(root, name))
                else:
                    source = os.path.join(root, name)
                    destination = root
                    self.verbose_print('moving {} to {} because its a dot file'.format(source, destination))
                    if not self.dry_run:
                        try:
                            shutil.move(source, destination)
                        except:
                            self.verbose_print('Unable to move dot file {}'.format(source))
                            pass
            for name in dirs:
                try:
                    if not self.dry_run:
                        os.rmdir(os.path.join(root, name))
                        self.verbose_print(u'Deleting empty directory {0}'.format(os.path.join(root, name)))
                except:
                    pass
        return file_list

    def rename_files(self, drive, file_list):
        """
        Get all rename options that apply to this drive
        Loop through and get all the characters that need to be replaced
        Replace those characters and rename the files from the ifl_list passed in.
        """
        renames = TvRenameOptions.objects.filter(applies_to=drive)
        replace_these = []
        for source_file in file_list:
            file_path, filename = os.path.split(source_file)
            new_filename = filename.lower().replace('-','.').replace(' ','.')
            for rename in renames:
                replace_these.extend(rename.characters_to_replace.split(','))
                while any(replace_this in new_filename for replace_this in replace_these):
                    for replace_this in replace_these:
                        if replace_this:
                            new_filename = new_filename.replace(replace_this, rename.replace_with_this)
                # one off replacements
                new_filename = new_filename.replace('\'', '')
                replace_these = []
            if filename != new_filename:
                self.verbose_print(u'Renaming...\n{0}\n{1}\n'.format(normalize('NFKD',filename).encode('ascii','ignore'), normalize('NFKD',new_filename).encode('ascii','ignore')))
                if not self.dry_run:
                    try:
                        os.rename(file_path+'/'+filename, file_path+'/'+new_filename)
                    except:
                        import ipdb; ipdb.set_trace()

    def delete_or_accept_filesize(self, movie_file, drive):
        if not drive.remove_small_files:
            return True
        acceptable_filesize = 70000000
        try:
            file_size = os.path.getsize(movie_file)
        except:
            self.verbose_print(u'unable to get file size of {0}'.format(movie_file))
            return False
        if file_size < acceptable_filesize:
            self.verbose_print(u'File {0} is {1} MB. Deleting it...'.format(movie_file, str(int(str(file_size))/(1024*1024))))
            if not self.dry_run:
                os.remove(movie_file)
            return False
        return True

    def determine_show_name(self, file_path, file_name):
        show_name = ''
        season_regex = "(.s\d{2}e\d{2}.)"
        season = re.findall(season_regex, file_name)
        if season:
            return file_name.split(season[0])[0].replace('.', ' '), file_name

        season_regex = "(.s\d{2}.)"
        season = re.findall(season_regex, file_name)
        if season:
            return file_name.split(season[0])[0].replace('.', ' '), file_name

        #Usual filenames failed.. rename the file
        season_regex = "(\.\d{3}\.)"
        season = re.findall(season_regex, file_name)
        if season:
            season_string = '.s0' + season[0][1] + 'e' + season[0][2] + season[0][3] + '.'
            new_filename = file_name.replace(season[0],season_string)
            self.verbose_print(u'Renaming...\n{0}\n{1}\n'.format(file_name, new_filename))
            if not self.dry_run:
                os.rename(os.path.join(file_path, file_name), os.path.join(file_path, new_filename))
            return file_name.split(season[0])[0].replace('.', ' '), new_filename

        season_regex = "(\.\d{1}x\d{1,2}\.)"
        season = re.findall(season_regex, file_name)
        if season:
            season_string = '.s0' + season[0][1] + 'e' + season[0][3] + season[0][4] + '.'
            new_filename = file_name.replace(season[0],season_string)
            self.verbose_print(u'Renaming...\n{0}\n{1}\n'.format(file_name, normalize('NFKD',new_filename).encode('ascii','ignore')))
            if not self.dry_run:
                os.rename(os.path.join(file_path, file_name), os.path.join(file_path, new_filename))
            return file_name.split(season[0])[0].replace('.', ' '), new_filename

        season_regex = "(\.\d{2}x\d{2}\.)"
        season = re.findall(season_regex, file_name)
        if season:
            season_string = '.s' + season[0][1] + season[0][2] + 'e' + season[0][4] + season[0][5] + '.'
            new_filename = file_name.replace(season[0],season_string)
            self.verbose_print(u'Renaming...\n{0}\n{1}\n'.format(file_name, normalize('NFKD',new_filename).encode('ascii','ignore')))
            if not self.dry_run:
                os.rename(os.path.join(file_path, file_name), os.path.join(file_path, new_filename))
            return file_name.split(season[0])[0].replace('.', ' '), new_filename

        season_regex = "(\.\d{2}x\d{1}\.)"
        season = re.findall(season_regex, file_name)
        if season:
            season_string = '.s' + season[0][1] + season[0][2] + 'e0' + season[0][4] + '.'
            new_filename = file_name.replace(season[0],season_string)
            self.verbose_print(u'Renaming...\n{0}\n{1}\n'.format(file_name, normalize('NFKD',new_filename).encode('ascii','ignore')))
            if not self.dry_run:
                os.rename(os.path.join(file_path, file_name), os.path.join(file_path, new_filename))
            return file_name.split(season[0])[0].replace('.', ' '), new_filename

        season_regex = "(\.\d{4}\.)"
        season = re.findall(season_regex, file_name)
        if season and int(season[0].replace('.','')) < 1950:
            season_string = '.s' + season[0][1] + season[0][2] + 'e' + season[0][3] + season[0][4] + '.'
            new_filename = file_name.replace(season[0],season_string)
            self.verbose_print(u'Renaming...\n{0}\n{1}\n'.format(file_name, normalize('NFKD',new_filename).encode('ascii','ignore')))
            if not self.dry_run:
                os.rename(os.path.join(file_path, file_name), os.path.join(file_path, new_filename))
            return file_name.split(season[0])[0].replace('.', ' '), new_filename
        return show_name, file_name

    def determine_season(self, file_name):
        season_regex = 's(\d{4})'
        season = re.findall(season_regex, file_name)
        if season:
            return int(season[0])
        season_regex = 's(\d{3})'
        season = re.findall(season_regex, file_name)
        if season:
            return int(season[0])
        season_regex = 's(\d{2})'
        season = re.findall(season_regex, file_name)
        if season:
            return int(season[0])
        season_regex = 's(\d{1})'
        season = re.findall(season_regex, file_name)
        if season:
            return int(season[0])
        return 0

    def determine_episode(self, file_name):
        episode_regex = 'e(\d{2})'
        episode = re.findall(episode_regex, file_name)
        if episode:
            return episode

        episode_regex = 'e(\d{1})'
        episode = re.findall(episode_regex, file_name)
        if episode:
            return episode

        return [0]

    def determine_quality(self, file_name):
        quality_regex = '(\d{3,4}p)'
        quality = re.findall(quality_regex, file_name)
        if quality:
            return quality[0]
        quality_regex = '(dvdrip|dvd)'
        quality = re.findall(quality_regex, file_name)
        if quality:
            return 'dvdrip'
        quality_regex = '(hdtv)'
        quality = re.findall(quality_regex, file_name)
        if quality:
            return 'hdrip'
        quality_regex = '(dsrip|dsr)'
        quality = re.findall(quality_regex, file_name)
        if quality:
            return 'dsrip'
        quality_regex = '(pdtv)'
        quality = re.findall(quality_regex, file_name)
        if quality:
            return 'pdtv'
        quality_regex = '(tvrip)'
        quality = re.findall(quality_regex, file_name)
        if quality:
            return 'tvrip'
        quality_regex = '(vcd)'
        quality = re.findall(quality_regex, file_name)
        if quality:
            return 'vcd'
        return 'unknown'

    def move_and_catalog_files(self, file_list, drive):
        destination = drive.path
        if drive.destination:
            destination = drive.destination
        cataloged = []
        if self.catalog:
            if self.catalog == 'update':
                self.verbose_print(u'Catalog - update specified so just updating new and deleted tv_shows from {0}'.format(drive.name))
                cataloged = list(TvCatalog.objects.filter(drive=drive).values_list('file_name', flat=True).distinct())
            else:
                self.verbose_print(u'Catalog option specified does not make sense. Do you know how to type?')

        for source_file in file_list:
            file_path, file_name = os.path.split(source_file)
            file_extension = file_name[-3:]

            if self.catalog and file_name in cataloged:
                cataloged.remove(file_name)
            elif file_name not in cataloged:
                show_name, file_name = self.determine_show_name(file_path, file_name)
                source_file = os.path.join(file_path, file_name)
                if not self.delete_or_accept_filesize(source_file, drive):
                    pass
                elif show_name:
                    if not drive.show_names_lower_case:
                        show_name = show_name.title()
                    show_folder = os.path.join(destination, show_name)
                    final_destination = os.path.join(show_folder,file_name)
                    if not os.path.exists(show_folder):
                        self.verbose_print(u'{0} is a new show.\nCreating Directory -->{1}'.format(show_name, show_folder))
                        if not self.dry_run:
                            os.makedirs(show_folder)
                    if source_file == final_destination:
                        #self.verbose_print(u'Source and destination are the same so moving on to the next file.')
                        pass
                    else:
                        self.verbose_print(u'Moving..\n{0}\n{1}\n'.format( source_file, final_destination))
                        if not self.dry_run:
                            shutil.move(source_file, final_destination)
                            source_file = final_destination
                    try:
                        if file_path + '/' != drive.path:
                            os.removedirs(file_path)
                            self.verbose_print(u'Removed directory {0}'.format(file_path))
                    except OSError:
                        pass

                    if self.catalog and file_extension not in self.catalog_extension_blacklist and drive.catalog:
                        if not self.dry_run:
                            show, created = TvShow.objects.get_or_create(name=show_name)
                            season = self.determine_season(file_name)
                            episodes = self.determine_episode(file_name)
                            quality = self.determine_quality(file_name)
                            file_size = os.path.getsize(source_file)

                            for episode in episodes:
                                try:
                                    TvCatalog.objects.create(
                                        show=show,
                                        drive=drive,
                                        season=season,
                                        episode=int(episode),
                                        quality=quality,
                                        file_name=file_name,
                                        file_size=file_size,
                                        file_path=source_file
                                    )
                                except:
                                    print "could not move {0}/{1}".format(file_path, file_name)
                            if created:
                                show.episodes = len(episodes)
                                show.save()
                            else:
                                show.episodes = show.episodes + len(episodes)
                                show.save()
                else:
                    self.verbose_print(u'Unable to determine show name for\n{0}\nmoving this file to 000\n'.format(source_file))
                    show_folder = os.path.join(destination, '000')
                    final_destination = os.path.join(show_folder,file_name)
                    if not self.dry_run:
                        if not os.path.exists(show_folder):
                            os.makedirs(show_folder)
                        shutil.move(source_file, final_destination)
                        source_file = final_destination

        for deleted_episode in cataloged:
            self.verbose_print(u'Deleting tv_show - {0} - from catalog since it no longer exists'.format(deleted_episode))
            if not self.dry_run:
                db_deleted_episodes = TvCatalog.objects.filter(drive=drive, file_name=deleted_episode)
                for db_deleted_episode in db_deleted_episodes:
                    db_deleted_episode.delete()

