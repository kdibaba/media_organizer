# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tv_organizer', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tvrenameoptions',
            name='replace_with_this',
            field=models.TextField(),
        ),
    ]
