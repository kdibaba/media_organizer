# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TvCatalog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('season', models.IntegerField(null=True, blank=True)),
                ('episode', models.IntegerField(null=True, blank=True)),
                ('file_name', models.CharField(unique=True, max_length=200)),
                ('file_size', models.CharField(max_length=200)),
                ('file_path', models.CharField(max_length=300)),
                ('quality', models.CharField(max_length=200, null=True, blank=True)),
                ('duplicate', models.BooleanField(default=False)),
                ('to_delete', models.NullBooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='TvDrive',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('path', models.CharField(max_length=100)),
                ('destination', models.CharField(max_length=200, null=True, blank=True)),
                ('capacity', models.CharField(max_length=200, null=True, blank=True)),
                ('free_space', models.CharField(max_length=200, null=True, blank=True)),
                ('date', models.DateTimeField(auto_now_add=True, null=True)),
                ('enabled', models.BooleanField(default=True)),
                ('last_message', models.CharField(max_length=300, null=True, blank=True)),
                ('description', models.TextField(help_text=b'Describe what this source and destination combo are supposed to achieve', null=True, blank=True)),
                ('remove_small_files', models.BooleanField(default=False)),
                ('show_names_lower_case', models.BooleanField(default=False)),
                ('catalog', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='TvMissing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('season', models.IntegerField(null=True, blank=True)),
                ('episode', models.IntegerField(null=True, blank=True)),
                ('quality', models.CharField(max_length=200, null=True, blank=True)),
                ('missing_season', models.BooleanField(default=False)),
                ('drive', models.ForeignKey(to='tv_organizer.TvDrive')),
            ],
        ),
        migrations.CreateModel(
            name='TvRenameOptions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
                ('characters_to_replace', models.CharField(help_text=b'Comma seperated list to replace. NO SPACES between commas and no trailing commas.', max_length=200, null=True, blank=True)),
                ('replace_with_this', models.CharField(max_length=200)),
                ('applies_to', models.ManyToManyField(to='tv_organizer.TvDrive', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='TvShow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=200)),
                ('seasons', models.IntegerField(null=True, blank=True)),
                ('episodes', models.IntegerField(null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='tvmissing',
            name='show',
            field=models.ForeignKey(to='tv_organizer.TvShow'),
        ),
        migrations.AddField(
            model_name='tvcatalog',
            name='drive',
            field=models.ForeignKey(to='tv_organizer.TvDrive'),
        ),
        migrations.AddField(
            model_name='tvcatalog',
            name='show',
            field=models.ForeignKey(to='tv_organizer.TvShow'),
        ),
    ]
