from django.db import models
from datetime import datetime
from django.utils import timezone

class TvDrive(models.Model):
    """ Tv Drive """
    name = models.CharField(max_length=50)
    path = models.CharField(max_length=100)
    destination = models.CharField(max_length=200, blank=True, null=True)
    capacity = models.CharField(max_length=200, null=True, blank=True)
    free_space = models.CharField(max_length=200, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    enabled = models.BooleanField(default=True)
    last_message = models.CharField(max_length=300, null=True, blank=True)
    description = models.TextField(help_text='Describe what this source and destination combo are supposed to achieve', null=True, blank=True)
    remove_small_files = models.BooleanField(default=False)
    show_names_lower_case = models.BooleanField(default=False)
    catalog = models.BooleanField(default=True)

    def __unicode__(self):
        return str(self.name)

class TvRenameOptions(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    characters_to_replace = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        help_text='Comma seperated list to replace. NO SPACES between commas and no trailing commas.')
    replace_with_this = models.TextField()
    applies_to = models.ManyToManyField(TvDrive, blank=True)

    def __unicode__(self):
        return str(self.characters_to_replace) + str(' replace with ') + str(self.replace_with_this)

class TvShow(models.Model):
    """ Tv shows name """
    name = models.CharField(max_length=200, unique=True)
    seasons = models.IntegerField(null=True, blank=True)
    episodes = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return str(self.name)

class TvCatalog(models.Model):
    """ Tv shows catalog """
    show = models.ForeignKey(TvShow)
    drive = models.ForeignKey(TvDrive)
    season = models.IntegerField(null=True, blank=True)
    episode = models.IntegerField(null=True, blank=True)
    file_name = models.CharField(max_length=200, unique=True)
    file_size = models.CharField(max_length=200)
    file_path = models.CharField(max_length=300)
    quality = models.CharField(max_length=200, null=True, blank=True)
    duplicate = models.BooleanField(default=False)
    to_delete = models.NullBooleanField()

    def __unicode__(self):
        return str(self.file_name)

class TvMissing(models.Model):
    """ Tv shows catalog """
    show = models.ForeignKey(TvShow)
    drive = models.ForeignKey(TvDrive)
    season = models.IntegerField(null=True, blank=True)
    episode = models.IntegerField(null=True, blank=True)
    quality = models.CharField(max_length=200, null=True, blank=True)
    missing_season = models.BooleanField(default=False)

    def __unicode__(self):
        return str(self.show)

