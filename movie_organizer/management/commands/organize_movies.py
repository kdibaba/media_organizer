# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from movie_organizer.models import (MovieOrganizer,
                                    BLURAY,
                                    WEB_RIP,
                                    DVD,
                                    HD,
                                    CAM,
                                    UNKNOWN,
                                    MovieRenameOptions,
                                    MovieHyphenation,
                                    MovieDrive,
                                    MovieCatalog,
                                    DeleteFileTypes,)
from optparse import make_option
from django.utils import timezone
from unicodedata import normalize
from django.db.models import Count

import os, shutil, re

RESOLUTION_SCORES = {
    '1080p': 10,
    '720p': 8,
    '576p': 6,
    '480p': 4,
    'unknown': 2,
}

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
         make_option('--verbose',
             dest='verbose',
             action='store_true',
             help='Verbose mode. Display all actions on screen.'),
         make_option('--dry-run',
             dest='dry_run',
             action='store_true',
             help='Dry run.. nothing is executed.'),
         make_option('--catalog',
             dest='catalog',
             action='store',
             help='Catalog the movies in the collection'),
         make_option('--find-duplicates',
             dest='find_duplicates',
             action='store_true',
             help='Find and mark duplicates'),
         make_option('--delete-duplicates',
             dest='delete_duplicates',
             action='store_true',
             help='Delete duplicates'),
         )
    def handle(self, *args, **options):
        self.verbose = options.get('verbose', False)
        self.dry_run = options.get('dry_run', False)
        self.catalog = options.get('catalog', False)
        self.find_duplicates = options.get('find_duplicates', False)
        self.delete_duplicates = options.get('delete_duplicates', False)
        self.catalog_extension_blacklist = ['idx', 'srt', 'sub']

        self.quality_score = {
            '1080p':10,
            '720p': 9,
            'dvdrip': 8,
            '576p': 7,
            '480p': 6,
            'unknown': 5,
        }

        if self.delete_duplicates and not self.catalog:
            raise CommandError("can't delete without cataloging....bad things can happen")

        self.verbose_print('Getting all settings from the organizer')
        movie_settings = MovieOrganizer.objects.filter(enabled=True).exclude(name='nzb')

        self.verbose_print('Got {0} setting(s) and processing them now\n'.format(movie_settings.count()))
        if movie_settings:
            for movie_setting in movie_settings:
                self.delete_invalid_files(movie_setting.source)
                files_to_rename = self.get_file_names(movie_setting.source)
                if files_to_rename:
                    self.rename_files(movie_setting, files_to_rename)
                    files_to_move = self.get_file_names(movie_setting.source)
                    if files_to_move:
                        self.move_files(files_to_move, movie_setting)
                    else:
                        self.verbose_print('Found no files to move. Very unlikely.')
                        movie_setting.last_message = 'Found no files to move. Very unlikely'
                        movie_setting.save()
                else:
                    self.verbose_print('Found no files to process for setting: {0}'.format(movie_setting.name))
                    movie_setting.last_message = 'No files to process'
                    movie_setting.save()
        else:
            self.verbose_print('No Settings to process')

        if self.catalog:
            drives = MovieDrive.objects.all()
            for drive in drives:
                if drive.catalog:
                    self.update_drive_stats(drive)
                    self.verbose_print('Cataloging {0}'.format(drive.drive_name))
                    path_list = self.get_file_names_to_catalog(drive.drive_path)
                    movies_list = []
                    if self.catalog == 'new':
                        self.verbose_print('Catalog - new specified so deleting all previous movies in {0}'.format(drive.drive_name))
                        if not self.dry_run:
                            MovieCatalog.objects.filter(drive=drive).delete()
                    elif self.catalog =='update':
                        self.verbose_print('Catalog - update specified so just updating new and deleted movies from {0}'.format(drive.drive_name))
                        movies_list = list(MovieCatalog.objects.filter(drive=drive).values_list('file_name', flat=True))
                    for path in path_list:
                        file_path, movie_file_name = os.path.split(path)
                        file_extension = movie_file_name[-3:]
                        if movie_file_name not in movies_list and file_extension not in self.catalog_extension_blacklist:
                            drive_path, movie_name = os.path.split(file_path)
                            file_size = os.path.getsize(path)
                            resolution = self.determine_resolution(movie_file_name)
                            quality = self.determine_quality(movie_file_name)

                            if not self.dry_run:
                                MovieCatalog.objects.create(
                                    drive=drive,
                                    movie_name=movie_name,
                                    file_name=movie_file_name,
                                    file_size=file_size,
                                    file_path=path,
                                    resolution=resolution,
                                    quality=quality,
                                )
                        else:
                            movies_list.remove(movie_file_name)
                    for deleted_movie in movies_list:
                        self.verbose_print('Deleting movie - {0} - from catalog since it no longer exists'.format(deleted_movie))
                        if not self.dry_run:
                            db_deleted_movies = MovieCatalog.objects.filter(drive=drive, file_name=deleted_movie)
                            for db_deleted_movie in db_deleted_movies:
                                db_deleted_movie.delete()

        if self.find_duplicates:
            self.verbose_print('Looking for and marking duplicates')
            self.find_mark_duplicates()

        if self.delete_duplicates:
            self.delete_marked_duplicates();

    def find_mark_duplicates(self):
        MovieCatalog.objects.filter(to_delete=True).update(to_delete=None)
        MovieCatalog.objects.filter(duplicate=True).update(duplicate=False)
        all_movies = MovieCatalog.objects.filter(to_delete=None)
        all_duplicate_movies = MovieCatalog.objects.filter(
                                    to_delete=None).values(
                                    'movie_name').annotate(
                                    Count('id')).order_by().filter(
                                    id__count__gt=1).values_list(
                                    'movie_name',
                                    flat=True)
        #all_movies = MovieCatalog.objects.filter(movie_name__in=all_duplicate_movies)
        duplicate_exceptions = ['part','cd']
        for movie in all_movies:
            if movie.movie_name in all_duplicate_movies:
                skip_movie = False
                for exception in duplicate_exceptions:
                    if exception in movie.file_name and exception not in movie.movie_name:
                        skip_movie = True
                duplicate_movies = MovieCatalog.objects.filter(movie_name=movie.movie_name)
                if duplicate_movies.count() > 1 and skip_movie == False:
                    duplicate_movies.update(duplicate=True)
                    movie_to_delete = None

                    for movie in duplicate_movies:
                        if not movie_to_delete:
                            movie_to_delete = movie

                        movie_to_delete = self.compare_movie_quality(movie, movie_to_delete)

                    if movie_to_delete.to_delete == None:
                        movie_to_delete.to_delete = True
                        movie_to_delete.save()
                else:
                    MovieCatalog.objects.filter(movie_name=movie.movie_name).update(duplicate=False)


    def compare_movie_filesize(self, movie_one, movie_two):
        '''
        return the movie with the smaller filesize
        if they are equal, return the one with a higher PK.
        '''
        if movie_one.file_size == movie_two.file_size:
            if movie_one.id < movie_two.id:
                return movie_two
            else:
                return movie_one

        else:
            if movie_one.file_size < movie_two.file_size:
                return movie_one
            else:
                return movie_two

    def compare_movie_resolution(self, movie_one, movie_two):
        '''
        compare two movies based on resolution
        return the lower quality resolution or None of they are equal.
        '''
        if movie_one.resolution == movie_two.resolution:
            return self.compare_movie_filesize(movie_one, movie_two)
        else:
            if RESOLUTION_SCORES[movie_one.resolution] > RESOLUTION_SCORES[movie_two.resolution]:
                return movie_two
            else:
                return movie_one


    def compare_movie_quality(self, movie_one, movie_two):
        '''
        return the movie with the lower quality
        or None if quality is equal or unkown.
        '''
        if movie_one.quality == movie_two.quality:
            return self.compare_movie_resolution(movie_one, movie_two)
        else:
            if movie_one.quality < movie_two.quality:
                return movie_one
            else:
                return movie_two

    def delete_marked_duplicates(self):
        movies = MovieCatalog.objects.filter(to_delete=True)
        for movie in movies:
            self.verbose_print('Deleting duplicate movie {0}'.format(movie.file_name))
            if not self.dry_run:
                os.remove(movie.file_path)
                movie.delete()

    def update_drive_stats(self, drive):
        self.verbose_print('update drive information for {0}'.format(drive.drive_name))
        drive_info = os.statvfs(drive.drive_path)
        capacity = drive_info.f_blocks * drive_info.f_frsize
        free_space = drive_info.f_bavail * drive_info.f_frsize
        self.verbose_print('{0} has {1} space out of {2}'.format(drive.drive_name, free_space, capacity))

        drive.drive_capacity = capacity
        drive.drive_free_space = free_space
        if not self.dry_run:
            drive.save()
        return

    def verbose_print(self, message):
        if self.verbose:
            print message

    def delete_invalid_files(self, directory):
        """
        all files whose extensions are recorded in the deletefiletypes model as being invalid.
        """
        invalid_file_types = DeleteFileTypes.objects.values_list('extension', flat=True)
        for root, dirs, files in os.walk(directory):
            for name in files:
                extension = name.rsplit('.')[-1]
                if extension.lower() in invalid_file_types:
                    self.verbose_print('deleting {0}'.format(normalize('NFKD', os.path.join(root,name)).encode('ascii','ignore')))
                    if not self.dry_run:
                        os.remove(os.path.join(root, name))

    def get_file_names(self, directory):
        """
        Return list of all files found in directory passed in.
        Recursively find all files in all sub directories as well.
        Delete any empty folders found.
        """
        file_list = []
        for root, dirs, files in os.walk(directory):
            for name in files:
                file_list.append(os.path.join(root, name))
            for name in dirs:
                try:
                    os.rmdir(os.path.join(root, name))
                    self.verbose_print('Deleting empty directory {0}'.format(os.path.join(root, name)))
                except:
                    pass
        return file_list

    def get_file_names_to_catalog(self, directory):
        """
        Return list of all files found in directory passed in.
        Recursively find all files in all sub directories as well.
        Delete any empty folders found.
        Return on files that have a video extension.
        """
        extensions = ['mkv','avi','mp4']
        file_list = []
        for root, dirs, files in os.walk(directory):
            for name in files:
                extension = name.rsplit('.')[-1]
                if extension in extensions:
                    file_list.append(os.path.join(root, name))
            for name in dirs:
                try:
                    os.rmdir(os.path.join(root, name))
                    self.verbose_print('Deleting empty directory {0}'.format(os.path.join(root, name)))
                except:
                    pass
        return file_list

    def rename_files(self, rename_setting, file_list):
        """
        Get all rename options that apply to this rename_setting
        Loop through and get all the characters that need to be replaced
        Replace those characters and rename the files from the file_list passed in.
        """
        renames = MovieRenameOptions.objects.filter(applies_to=rename_setting)
        if not renames:
            self.verbose_print('There are no rename options for rename_setting: {0}'.format(rename_setting.name))
            return
        for source_file in file_list:
            if self.delete_or_accept_filesize(source_file):
                file_path, file_name = os.path.split(source_file)
                movie_name = self.determine_movie_name(file_name)
                new_filename = file_name.lower().replace('-','.').replace(' ','.')
                extension = new_filename.rsplit('.')[-1]
                new_filename = new_filename.replace(extension, '')
                new_filename = self.hyphenate_movies(new_filename, rename_setting)
                for rename in renames:
                    replace_these = rename.characters_to_replace.split(',')
                    while any(replace_this in new_filename for replace_this in replace_these):
                        for replace_this in replace_these:
                            if replace_this:
                                new_filename = new_filename.replace(replace_this, rename.replace_with_this)
                    # one off replacements
                    new_filename = new_filename.replace('\'', '')
                if new_filename[-1] == '-' or new_filename[-1] == '.':
                    new_filename = new_filename[:-1] + new_filename[-1:].replace('-', '.') + extension
                else:
                    new_filename = new_filename + extension
                if new_filename[0] == '.' or new_filename[0] == '-':
                    new_filename = new_filename[1:]
                if file_name != new_filename:
                    try:
                        self.verbose_print('Renaming...\n{0}\n{1}\n'.format(file_name, normalize('NFKD', new_filename).encode('ascii','ignore')))
                        if not self.dry_run:
                            os.rename(source_file, os.path.join(file_path, new_filename))
                    except:
                        pass
                    pass

    def determine_movie_name(self, file_name):
        year_regex = "\d{4}-|\d{4}\."
        year = re.findall(year_regex, file_name)
        movie_title = file_name.split('-')[0].replace('.', ' ')
        movie_title = movie_title.replace(' mkv', '').replace(' avi', '')
        if year:
            proper_year = year[0].replace('-','').replace('.','')
            if proper_year in movie_title:
                return movie_title
            else:
                return movie_title + ' ' + proper_year
        return movie_title

    def determine_quality(self, file_name):
        lower_file_name = file_name.lower()
        if 'bluray' in lower_file_name:
            return BLURAY
        elif 'web.rip' in lower_file_name or 'web.dl' in lower_file_name:
            return WEB_RIP
        elif 'hdrip' in lower_file_name or 'hd.rip' in lower_file_name:
            return HD
        elif 'dvd' in lower_file_name:
            return DVD
        elif 'cam' in lower_file_name:
            return CAM
        else:
            return UNKNOWN

    def determine_resolution(self, file_name):
        resolution_regex = '(\d{3,4}p)'
        resolution = re.findall(resolution_regex, file_name)
        if resolution:
            return resolution[0]
        return 'unknown'

    def hyphenate_movies(self, filename, movie_setting):
        options = MovieHyphenation.objects.filter(applies_to=movie_setting)
        if not options:
            self.verbose_print('There are no hyphenations for movie_setting: {0}'.format(movie_setting.name))
            return filename
        for option in options:
            strings_to_hyphenate = option.strings_to_hyphenate.split(',')
            for to_hyphenate in strings_to_hyphenate:
                if to_hyphenate:
                    filename = filename.replace('.'+to_hyphenate+'.', '-'+to_hyphenate+'-')
                    filename = filename.replace('.'+to_hyphenate+'-', '-'+to_hyphenate+'-')
                    filename = filename.replace('-'+to_hyphenate+'.', '-'+to_hyphenate+'-')
                    filename = filename.replace('-'+to_hyphenate+'-', '-'+to_hyphenate+'-')
        return filename

    def delete_or_accept_filesize(self, movie_file):
        acceptable_filesize = 200000000
        acceptable_extensions = ['srt','idx','sub']
        try:
            file_size = os.path.getsize(movie_file)
        except:
            self.verbose_print('unable to get file size of {0}'.format(movie_file))
            return False
        if file_size < acceptable_filesize:
            extension = movie_file.rsplit('.')[-1]
            if extension not in acceptable_extensions:
                self.verbose_print('File {0} is {1} MB. Deleting it...'.format(movie_file, str(int(file_size)/(1024*1024))))
                if not self.dry_run:
                    os.remove(movie_file)
                return False
        return True

    def move_files(self, file_list, movie_setting):
        source = movie_setting.source
        destination = movie_setting.destination
        for file_ in file_list:
            source_file = normalize('NFKD', file_).encode('ascii', 'ignore')
            file_path, file_name = os.path.split(source_file)
            use_this_file_name = file_name
            if file_path + '/' != source:
                #check if the folder has a better name for the movie than the file_name
                _, folder_name = os.path.split(file_path)
                if len(folder_name) > len(file_name):
                    file_extension = file_name.rsplit('.')[-1]
                    use_this_file_name = folder_name + '.' + file_extension
            movie_name = self.determine_movie_name(use_this_file_name)
            if movie_name:
                    movie_folder = destination + movie_name
                    final_destination = os.path.join(movie_folder,use_this_file_name)
                    if not os.path.exists(movie_folder):
                        self.verbose_print('Creating Directory -->{0}'.format(movie_folder))
                        if not self.dry_run:
                            os.makedirs(movie_folder)
                    if source_file == final_destination:
                        #self.verbose_print('Source and destination are the same meaning file is already in the right place. Moving on to the next file.')
                        pass
                    else:
                        self.verbose_print('Moving..\n{0}\n{1}\n'.format(source_file, final_destination))
                        if not self.dry_run:
                            shutil.move(source_file, final_destination)
                    try:
                        os.removedirs(file_path)
                    except OSError:
                        pass
            else:
                self.verbose_print('Unable to determine movie name for\n{0}\nAborting move of this file\n'.format(source_file))

    def handle_nzbs(self, nzb_settings):
        nzb_file_list = self.get_file_names(nzb_settings.source)
        movie_list = MovieCatalog.objects.values_list('movie_name', flat=True)
        check_similar_files = {}
        for nzb_file in nzb_file_list:
            nzb_path, nzb_filename = os.path.split(nzb_file)
            nzb_movie_name = self.determine_movie_name(nzb_filename)
            if nzb_movie_name in movie_list:
                self.verbose_print('{0} is a duplicate. Moving it away'.format(nzb_filename))
                if not self.dry_run:
                    shutil.move(nzb_file, os.path.join(nzb_settings.destination, nzb_filename))
            else:
                if nzb_movie_name not in check_similar_files.keys():
                    check_similar_files[nzb_movie_name] = os.path.join(nzb_path, nzb_filename)
                else:
                    original_file_size = os.path.getsize(check_similar_files[nzb_movie_name])
                    new_file_size = os.path.getsize(nzb_file)
                    if original_file_size > new_file_size:
                        self.verbose_print('Moving {0} of size {1} to duplicates and keeping\n{2} of size {3}'.format(
                            nzb_file, new_file_size, check_similar_files[nzb_movie_name], original_file_size))
                        if not self.dry_run:
                            shutil.move(nzb_file, os.path.join(nzb_settings.destination, nzb_filename))
                    else:
                        self.verbose_print('Moving {0} of size {1} to duplicates and keeping\n{2} of size {3}'.format(
                            check_similar_files[nzb_movie_name], original_file_size, nzb_file, new_file_size))
                        if not self.dry_run:
                            temp_nzb_path, temp_nzb_filename = os.path.split(check_similar_files[nzb_movie_name])
                            shutil.move(check_similar_files[nzb_movie_name], os.path.join(nzb_settings.destination, temp_nzb_filename))
                            check_similar_files[nzb_movie_name] = nzb_file
