from django.http import HttpResponse, HttpResponseRedirect

from movie_organizer.models import MovieCatalog

from json import dumps
import re

def get_movies(request):
    #movie_list = MovieCatalog.objects.filter(file_size__gte=3000000000).values_list('movie_name', flat=True)
    movie_list = MovieCatalog.objects.values_list('movie_name', flat=True)
    #print len(movie_list)
    cleaned_movie_list = []
    for movie in movie_list:
        cleaned_movie_list.append(cleaned_movie_name(movie.lower()))
    return HttpResponse(dumps(cleaned_movie_list), content_type='application/json')


def cleaned_movie_name(movie_name):
    match_year_numbers = '[0-2][0-9][0-9][0-9]'
    match = re.search(match_year_numbers, movie_name)
    if match:
        return movie_name.replace(match.group(), '').rstrip()
    return movie_name
