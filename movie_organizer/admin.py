from django.contrib import admin
from django.db import models
from movie_organizer.models import (MovieOrganizer,
                                    MovieRenameOptions,
                                    MovieHyphenation,
                                    MovieDrive,
                                    MovieCatalog,
                                    DeleteFileTypes,)
import re

class MovieOrganizerAdmin(admin.ModelAdmin):
    list_display = ('name', 'source', 'destination', 'description','enabled')
admin.site.register(MovieOrganizer, MovieOrganizerAdmin)

class MovieRenameOptionsAdmin(admin.ModelAdmin):
    list_display = ('name',
                    'characters_to_replace',
                    'replace_with_this', )
admin.site.register(MovieRenameOptions, MovieRenameOptionsAdmin)

class MovieHyphenationAdmin(admin.ModelAdmin):
    list_display = ('name',
                    'strings_to_hyphenate',)
admin.site.register(MovieHyphenation, MovieHyphenationAdmin)

class MovieDriveAdmin(admin.ModelAdmin):
    list_display = ('drive_name',
                    'drive_path',
                    'capacity',
                    'free_space')

    def free_space(self, obj):
        if obj.drive_free_space:
            return str(int(obj.drive_free_space) / (1024*1024*1024)) + ' GB'
        else:
            return 'Not yet scanned'

    def capacity(self, obj):
        if obj.drive_capacity:
            return str(int(obj.drive_capacity) / (1024*1024*1024)) + ' GB'
        else:
            return 'Not yet scanned'
admin.site.register(MovieDrive, MovieDriveAdmin)

class MovieCatalogAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'drive',
                    'movie_name',
                    'cleaned_movie_name',
                    'movie_name_length',
                    'quality',
                    'readable_file_size',
                    'duplicate',
                    'to_delete',
                    'file_name',)

    search_fields = ['movie_name']
    actions = ['mark_to_delete', 'remove_delete_mark']
    list_max_show_all = 15000

    def mark_to_delete(self, request, queryset):
        for movie in queryset:
            movie.to_delete = True
            movie.save()
        self.message_user(request, 'successfully marked {0} movies for deletion'.format(len(queryset)))

    def remove_delete_mark(self, request, queryset):
        for movie in queryset:
            movie.to_delete = False
            movie.save()
        self.message_user(request, 'successfully removed mark from {0} movies for deletion'.format(len(queryset)))

    def readable_file_size(self, obj):
        if obj.file_size:
            return str(int(obj.file_size) / (1024*1024)) + ' MB'
        else:
            return 'Not yet scanned'

    def cleaned_movie_name(self, obj):
        match_year_numbers = '[0-2][0-9][0-9][0-9]'
        match = re.search(match_year_numbers, obj.movie_name)
        if match:
            return obj.movie_name.replace(match.group(), '').rstrip()
        else:
            return obj.movie_name

    def movie_name_length(self, obj):
        return 'length - {}$'.format(str(len(obj.file_name)))

admin.site.register(MovieCatalog, MovieCatalogAdmin)

class DeleteFileTypesAdmin(admin.ModelAdmin):
    list_display = ('extension',)
admin.site.register(DeleteFileTypes, DeleteFileTypesAdmin)
