from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('movie_organizer.views',
    url(r'admin/', include(admin.site.urls)),
    url(r'get_movies/$', 'get_movies', name='get_movies'),
)
