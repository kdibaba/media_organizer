# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('movie_organizer', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='moviecatalog',
            name='resolution',
            field=models.CharField(default=None, max_length=5),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='moviecatalog',
            name='quality',
            field=models.IntegerField(blank=True, null=True, choices=[(5, b'Bluray'), (4, b'Web Rip'), (3, b'HD Rip'), (2, b'DVD Rip'), (0, b'CAM / TS'), (1, b'Unknown')]),
        ),
    ]
