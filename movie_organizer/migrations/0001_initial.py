# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DeleteFileTypes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('extension', models.CharField(max_length=4)),
            ],
        ),
        migrations.CreateModel(
            name='MovieCatalog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('movie_name', models.CharField(max_length=200)),
                ('file_name', models.CharField(max_length=200)),
                ('file_size', models.CharField(max_length=200)),
                ('file_path', models.CharField(max_length=300)),
                ('quality', models.CharField(max_length=200, null=True, blank=True)),
                ('duplicate', models.BooleanField(default=False)),
                ('to_delete', models.NullBooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='MovieDrive',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('drive_name', models.CharField(max_length=50)),
                ('drive_path', models.CharField(max_length=100)),
                ('drive_capacity', models.CharField(max_length=200, null=True, blank=True)),
                ('drive_free_space', models.CharField(max_length=200, null=True, blank=True)),
                ('drive_date', models.DateTimeField(auto_now_add=True, null=True)),
                ('catalog', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='MovieHyphenation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'General name for the strings you would like to be hyphenated', max_length=50)),
                ('strings_to_hyphenate', models.CharField(help_text=b'Comma seperated list to replace. NO SPACES between commas and no trailing commas.', max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='MovieOrganizer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('source', models.CharField(help_text=b'the directory of the file before renaming and moving.', max_length=200)),
                ('destination', models.CharField(help_text=b'where to move the files to.', max_length=200)),
                ('last_message', models.CharField(help_text=b'Last message from attempt to move and rename', max_length=300, null=True, blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(help_text=b'Title for transfer configuration for target source and destination', max_length=50)),
                ('description', models.TextField(help_text=b'Describe what this source and destination combo are supposed to achieve')),
                ('enabled', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='MovieRenameOptions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
                ('characters_to_replace', models.CharField(help_text=b'Comma seperated list to replace. NO SPACES between commas and no trailing commas.', max_length=200, null=True, blank=True)),
                ('replace_with_this', models.CharField(max_length=15)),
                ('applies_to', models.ManyToManyField(to='movie_organizer.MovieOrganizer', blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='moviehyphenation',
            name='applies_to',
            field=models.ManyToManyField(to='movie_organizer.MovieOrganizer', blank=True),
        ),
        migrations.AddField(
            model_name='moviecatalog',
            name='drive',
            field=models.ForeignKey(to='movie_organizer.MovieDrive'),
        ),
    ]
