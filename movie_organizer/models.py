from django.db import models
from datetime import datetime
from django.utils import timezone

BLURAY = 5
WEB_RIP = 4
HD = 3
DVD = 2
UNKNOWN = 1
CAM = 0

MOVIE_QUALITY = (
    (BLURAY, 'Bluray'),
    (WEB_RIP, 'Web Rip'),
    (HD, 'HD Rip'),
    (DVD, 'DVD Rip'),
    (CAM, 'CAM / TS'),
    (UNKNOWN, 'Unknown'),
)

class MovieOrganizer(models.Model):
    source = models.CharField(max_length=200, help_text='the directory of the file before renaming and moving.')
    destination= models.CharField(max_length=200, help_text='where to move the files to.')
    last_message = models.CharField(max_length=300, null=True, blank=True, help_text='Last message from attempt to move and rename')
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=50, help_text='Title for transfer configuration for target source and destination')
    description = models.TextField(help_text='Describe what this source and destination combo are supposed to achieve')
    enabled = models.BooleanField(default=True)

    def __unicode__(self):
        return str(self.name)

class MovieRenameOptions(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    characters_to_replace = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        help_text='Comma seperated list to replace. NO SPACES between commas and no trailing commas.')
    replace_with_this = models.CharField(max_length=15)
    applies_to = models.ManyToManyField(MovieOrganizer, blank=True)

    def __unicode__(self):
        return str(self.characters_to_replace) + str(' replace with ') + str(self.replace_with_this)

class MovieHyphenation(models.Model):
    name = models.CharField(max_length=50, help_text='General name for the strings you would like to be hyphenated')
    strings_to_hyphenate = models.CharField(
        max_length=200,
        help_text='Comma seperated list to replace. NO SPACES between commas and no trailing commas.')
    applies_to = models.ManyToManyField(MovieOrganizer, blank=True)

    def __unicode__(self):
        return str(self.strings_to_hyphenate)


class MovieDrive(models.Model):
    """ Movie Drive """
    drive_name = models.CharField(max_length=50)
    drive_path = models.CharField(max_length=100)
    drive_capacity = models.CharField(max_length=200, null=True, blank=True)
    drive_free_space = models.CharField(max_length=200, null=True, blank=True)
    drive_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    catalog = models.BooleanField(default=True)

    def __unicode__(self):
        return str(self.drive_name)


class MovieCatalog(models.Model):
    """ Movie Name """
    drive = models.ForeignKey(MovieDrive)
    movie_name = models.CharField(max_length=200)
    file_name = models.CharField(max_length=200)
    file_size = models.CharField(max_length=200)
    file_path = models.CharField(max_length=300)
    quality = models.IntegerField(
        choices=MOVIE_QUALITY,
        null=True,
        blank=True,
    )
    resolution = models.CharField(max_length=10)
    duplicate = models.BooleanField(default=False)
    to_delete = models.NullBooleanField()

    def __unicode__(self):
        return str(self.file_name)

class DeleteFileTypes(models.Model):
    """ File Extensions to delete """
    extension = models.CharField(max_length=4)

