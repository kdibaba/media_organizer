# TO-DO #

johnny-to-do-list:

1. don't allow just delete, catalog MUST update.
2. maintain history of deleted items.
3. skip dotfiles for Movies like we do for TV.
4. Use FIle transfer log + quality + catalog to determine if we should transfer a file or not (only bring upgrades)
5. instead of actualy deleting files, lets do a "soft delete" where we just move them to a to-delete folder.

### Future Ideas ###

1. use bootstrap tables + django to make a dashboard
2. remove small files that are not subs (sample.avi)
3. break out code into more reusable functions
4. email updates about new stuff that has come in (from auto DL)

