from django.db import models
from django.utils.encoding import smart_str

class MediaType(models.Model):
    created_datetime = models.DateTimeField(null=True, auto_now_add=True)
    MEDIA_TYPES = [
            ('Movie', 'Movie'),
            ('TV-Show', 'TV-Show'),
            ('Anime Series', 'Anime Series'),
            ('Animated Movie', 'Animated Movie'),
            ('Books', 'Comics'),
            ('Games', 'Games'),
            ('Audio', 'Muisc'),
            ('Add-on', 'Subtitle-Track')]
    media_type = models.CharField(max_length=20, choices=MEDIA_TYPES)

    def __unicode__(self):
        return self.media_type


class UserSettings(models.Model):
    created_datetime = models.DateTimeField(null=True, auto_now_add=True)
    username = models.CharField(max_length=20)
    htpc_host = models.TextField()
    htpc_port = models.IntegerField(null=True, blank=True)
    server_port = models.IntegerField(null=True, blank=True)
    server_host = models.TextField()

    def __unicode__(self):
        return self.username


class FileTransferRecord(models.Model):
    created_datetime = models.DateTimeField(null=True, auto_now_add=True)
    filename = models.TextField()
    media_type = models.ForeignKey(MediaType)
    user = models.ForeignKey(UserSettings)

    def __unicode__(self):
        return self.filename


class MediaFileExtension(models.Model):
    extension = models.CharField(max_length=6)

    def __unicode__(self):
        return self.extension


class MediaLocation(models.Model):
    created_datetime = models.DateTimeField(null=True, auto_now_add=True)
    media_type = models.ForeignKey(MediaType)
    htpc_path_to_media = models.TextField()
    server_path_to_media = models.TextField()
    user = models.ForeignKey(UserSettings)
    sync_media = models.BooleanField(default = True)
    #allowed_file_extensions = models.ManyToManyField(MediaFileExtension)
    blacklisted_file_extensions = models.ManyToManyField(MediaFileExtension, blank=True)

    def blacklisted_media_extensions(self):
        blacklisted_media_extensions = []
        for extension in self.blacklisted_file_extensions.all():
            blacklisted_media_extensions.append(extension.extension)
        return blacklisted_media_extensions

    def __unicode__(self):
        return "{media_type},{user}".format(media_type=str(self.media_type), user=str(self.user))


class TransferableFile(models.Model):
    media_type = models.TextField()
    path = models.TextField()
    destination = models.TextField()

    def __unicode__(self):
        try:
            return smart_str("{media_type}, {path}".format(media_type=self.media_type, path=self.path))
        except:
            return ''
