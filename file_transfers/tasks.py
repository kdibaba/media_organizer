from __future__ import absolute_import

from datetime import datetime, timedelta
from file_transfers.models import FileTransferRecord, UserSettings, TransferableFile, MediaLocation, MediaType
from os import walk, path
from subprocess import call, CalledProcessError, PIPE, check_output
from celery.decorators import periodic_task
from celery import shared_task
from utils.decorators import single_instance_task
import pipes
import re

def quote(filename):
    filename = filename.replace('(', '\(')
    filename = filename.replace(')', '\)')
    filename = filename.replace('[', '\[')
    filename = filename.replace(']', '\]')
    filename = filename.replace(' ', '\ ')
    filename = filename.replace("'", "\\'")
    return filename

def pull_file(source_path, destination_path, user):
    """
    call rsync on a file
    """
    source_path = quote(source_path)
    try:
        try:
            #clean_source = pipes.quote(source_path.encode('utf8'))
            clean_source = source_path.encode('utf8')
        except UnicodeDecodeError:
            clean_source = source_path
        user_host_concat =  '{user}@{host}:"{source}"'.format(
                user=user.username,
                host=user.server_host,
                source=clean_source,
        )
        port = user.server_port or 22
        command_str = 'rsync -a --progress -e "ssh -p {}" {} {}'.format(
                port,
                user_host_concat,
                destination_path,
        )
        print command_str
        success = call(command_str, shell=True)
    except OSError:
        print "rsync executable not found"
        return False
    except CalledProcessError:
        print "there was an error trying to rsync {source_file}".format(
            source_file=source_path
        )
        return False
    except:
        return False
    if success != 0:
        return False
    else:
        return True

def pull_files_from_host():
    '''
    pull files from remote server to localhost
    '''
    user = UserSettings.objects.get(username='johnnyg')
    media_locations = MediaLocation.objects.filter(
        user=user,
        sync_media=True
    )
    media_to_sync = []
    for location in media_locations:
        #location_string = location.server_path_to_media.split('/')[-1]
        filelist = get_remote_filelist(location, user)
        filelist = filelist.split('\n')
        result = []
        for filename in filelist[1:]:
            #filename = filename.split(location_string +'/')[-1]
            try:
                filename = re.split('[0-9]+:[0-9]+:[0-9]+.',filename)[1]
            except IndexError:
                continue
            #filename = filename.split(' ')[-1]
            source_path = "{0}{1}".format(location.server_path_to_media, filename)
            if filename and '/' not in filename and validate_file_transfer(source_path, user):
                if pull_file(
                    source_path=source_path,
                    destination_path=location.htpc_path_to_media,
                    user=user,
                ):
                    FileTransferRecord.objects.create(
                        filename=source_path,
                        user=user,
                        media_type=location.media_type,
                    )
                    result.append("transfering: {source} --> {dest}".format(
                        source=source_path,
                        dest=location.htpc_path_to_media,
                    ))
    return result


def get_remote_filelist(location, user):
    '''
    retrieve list of files to pull from remote server
    '''
    try:
        user_host_concat =  '{user}@{host}:{destination}'.format(
                user=user.username,
                host=user.server_host,
                destination=location.server_path_to_media,
        )
        port = user.server_port or 21976
        command_str = 'rsync -re "ssh -p {}" {}'.format(
                port,
                user_host_concat,
        )
        filelist = check_output(command_str, shell=True)
    except OSError:
        print "rsync executable not found"
        return []
    except CalledProcessError:
        print "there was an error trying to get remote file list"
        return []
    except:
        return []
    return filelist

def force_push_files_to_host():
    '''
    sync files from remote host to yourself.
    '''
    result = []
    for user in UserSettings.objects.all():
        result.append( sync_media(user))
    return result

def push_files_to_host():
    '''
    sync files from remote host to yourself.
    '''
    result = []
    for user in UserSettings.objects.all():
        result.append( sync_media(user))
    return result

def sync_media(user):
    """
    get a list of videos, then sync them
    """
    media_locations = MediaLocation.objects.filter(
        user=user,
        sync_media=True
    )
    result = []
    for location in media_locations:
        media_to_sync = get_media_to_sync(location, user)
        for filename in media_to_sync:
            if sync_file(
                    source_path=filename.path,
                    destination_path=location.htpc_path_to_media,
                    user=user,
            ):
                log_transfered_file(filename, location, user)
                result.append("transfering: {source} --> {dest}".format(
                    source=filename,
                    dest=location.htpc_path_to_media,
                ))
    return result

def sync_file(source_path, destination_path, user):
    """
    call rsync on a file
    """
    try:
        user_host_concat =  '{user}@{host}:{destination}'.format(
                user=user.username,
                host=user.htpc_host,
                destination=destination_path
        )
        port = user.htpc_port or 22
        command = [
                'rsync',
                '-av',
                '--progress',
                #"-e 'ssh -p {port}'".format(port=port),
                source_path.encode('utf8'),
                user_host_concat,
        ]
        command_str = 'rsync -av --progress -e "ssh -p {}" {} {}'.format(
                port,
                pipes.quote(source_path.encode('utf8')),
                user_host_concat,
        )
        success = call(command_str, shell=True)
    except OSError:
        print "rsync executable not found"
        return False
    except CalledProcessError:
        print "there was an error trying to rsync {source_file}".format(
            source_file=source_path
        )
        return False
    except:
        return False
    if success != 0:
        return False
    else:
        return True

def log_transfered_file(filename, location, user):
    new_log_entry = FileTransferRecord(**
            {
                'filename': filename.path,
                'user': user,
                'media_type': location.media_type,
            }
    )
    new_log_entry.save()

def validate_file_transfer(filename, user):
    """
    Check and see if a file has been sent for the current user before
    """
    previous_transfers = FileTransferRecord.objects.filter(
        filename=filename,
        user=user
    )
    try:
        if previous_transfers.count() > 0:
            return False
        else:
            return True
    except Exception as e:
        print "failed to transfer: {0}".format(filename)
        return False

def get_media_to_sync(location, user):
    """
    recursivly get a list of all files from a specified location
    """
    media_files = []
    blacklisted_file_types = location.blacklisted_media_extensions()
    file_list = []
    for dp, dn, fn in walk(unicode(path.expanduser(location.server_path_to_media))):
        dp = dp.encode('utf8')
        for f in fn:
            f = f.encode('utf8')
            file_list.append(path.join(dp, f))
    for filename in file_list:
        if not any(extension in filename for extension in blacklisted_file_types):
            params = {
                'media_type': location.media_type,
                'path': filename,
                'destination': location.htpc_path_to_media,
            }

            if validate_file_transfer(filename, user):
                transferable_file = TransferableFile(**params)
                media_files.append(transferable_file)
    return media_files
