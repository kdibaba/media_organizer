from django.forms import TextInput, Textarea
from django.db import models
from file_transfers.models import FileTransferRecord, UserSettings, MediaType, MediaLocation, MediaFileExtension
from django.contrib import admin


class MediaFileExtensionAdmin(admin.ModelAdmin):
    list_display = ('extension',)
admin.site.register(MediaFileExtension, MediaFileExtensionAdmin)

class FileTransferRecordAdmin(admin.ModelAdmin):
    list_display = ('pk', 'user', 'filename', 'media_type', 'created_datetime')
    raw_id_fields = []
    inlines = []
admin.site.register(FileTransferRecord, FileTransferRecordAdmin)


class MediaLocationAdmin(admin.TabularInline):
    model = MediaLocation
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':'20'})},
        models.TextField: {'widget': Textarea(attrs={'rows':1, 'cols':40})},
    }



class UserSettingsAdmin(admin.ModelAdmin):
    list_display = ('pk', 'username', 'created_datetime')
    raw_id_fields = []
    search_fields = ['username']
    inlines = [MediaLocationAdmin]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':'20'})},
        models.TextField: {'widget': Textarea(attrs={'rows':1, 'cols':40})},
    }
    def queryset(self, request):
        return super(UserSettingsAdmin, self).queryset(request)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        kwargs['queryset'] = MediaLocation.objects
        return super(UserSettingsAdmin, self).formfield_for_foreignkey(db_field, request=request, **kwargs)
    
    def lookup_allowed(self, *options):
        return True

admin.site.register(UserSettings, UserSettingsAdmin)


class MediaTypeAdmin(admin.ModelAdmin):
    list_display = ['media_type',]
    raw_id_fields = []
    inlines = []
admin.site.register(MediaType, MediaTypeAdmin)
