from datetime import datetime
from django.core.management.base import BaseCommand
from optparse import make_option
from django.conf import settings
from file_transfers.models import FileTransferRecord, UserSettings, TransferableFile, MediaLocation, MediaType
from file_transfers.tasks import get_media_to_sync
from os import walk, path
from subprocess import call, CalledProcessError

class Command(BaseCommand):
    """
    this management command is used to sync files between two servers running
    rsync
    """

    option_list = BaseCommand.option_list + (
        make_option('--verbose', action='store_true', help=''),
        make_option('--dry-run', action='store_true', help=''),
        make_option('--push', action='store_true', help=''),
    )

    def handle(self, *args, **options):
        from ... import tasks
        if options.get('push'):
            tasks.push_files_to_host()
        else:
            tasks.pull_files_from_host()

